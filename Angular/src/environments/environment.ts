export const environment = {
  production: false,
  pokeapiUrl: 'https://pokeapi.co/api/v2',
  backendApiUrl: 'http://localhost:8080'
};