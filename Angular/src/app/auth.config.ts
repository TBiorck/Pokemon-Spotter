const authConfig = {
  redirect_uri: 'http://localhost:4200/login',
  client_id: 'c0e3e9f860d6651ea3e4600cbe5c4dd5fe13ba644062495ba9e2ad9c1217ee2a',
  scope: 'read_user openid profile email',
  response_type: 'token'
}

export default authConfig