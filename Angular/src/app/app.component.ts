import { Component } from '@angular/core';
import { AuthService } from './services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Angular';

  constructor(private auth: AuthService, private router: Router) {};

  // TODO: Delete this
  login() {
    this.auth.login()
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/anonymous']);
  }

  readLocalStorageValue(key) {
    return localStorage.getItem(key);
  }

  getRouterUrl() {
    return this.router.url;
  }
}
