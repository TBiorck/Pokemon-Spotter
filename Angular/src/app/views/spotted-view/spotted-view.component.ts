import { Component, OnInit } from '@angular/core';
import { PokeapiService } from '../../services/pokeapi/pokeapi.service'
import { BackendService } from 'src/app/services/backend/backend.service';

@Component({
  selector: 'app-spotted-view',
  templateUrl: './spotted-view.component.html',
  styleUrls: ['./spotted-view.component.scss']
})
export class SpottedViewComponent implements OnInit {
  public pokeblocks: any[] = [];

  private username = window.localStorage.getItem('user')


  constructor(private pokeapiService: PokeapiService, private backendService: BackendService) { }

  async ngOnInit() {
    const spottedPokemons = await this.getSpottedPokemonsFromDatabase()

    this.loadDataToPokeBlocks(spottedPokemons)
  }

  private async getSpottedPokemonsFromDatabase() {
    try {
      const response: any = await this.backendService.getUser(this.username)
      return response.spottedPokemon
    } 
    catch (error) {
      console.error(error);
    }
  }

  private async loadDataToPokeBlocks(spottedPokemons) {
    const minPokemonId = 1
    const maxPokemonId = 151    
    
    const setOfSpottedPokemons = this.getsetOfSpottedPokemons(spottedPokemons)
    
    for (let i = minPokemonId; i <= maxPokemonId; i++) {      
      const result: any = await this.pokeapiService.getPokemonByNumber(i)

      // If the set of spotted pokemons contains the current value of i (or, i === id of a spotted pokemon), then seen is set to true
      result.seen = setOfSpottedPokemons[i] ? true : false

      if (result.seen) {
        result.female = setOfSpottedPokemons[i].foundFemale
        result.male = setOfSpottedPokemons[i].foundMale
        result.shiny = setOfSpottedPokemons[i].foundShiny
        result.latitude = setOfSpottedPokemons[i].firstFoundLatitude
        result.longitude = setOfSpottedPokemons[i].firstFoundLongitude
        
        const dateArray = setOfSpottedPokemons[i].firstFoundDate
        result.date = `${dateArray[0]}-${dateArray[1]}-${dateArray[2]} ${dateArray[3]}:${dateArray[4]}`

      }
      // Add result to pokeblocks, which is passed to the component pokeblock in order to display the data
      this.pokeblocks.push(result)  
    }
  }

  // Save pokemons as key value pair where the key is the pokemon ID and the value is the pokemon
  getsetOfSpottedPokemons(spottedPokemons): Object {
    const setOfSpottedPokemons = {}
    for (const pokemon of spottedPokemons) {
      setOfSpottedPokemons[pokemon.pokemonId] = pokemon
    }

    return setOfSpottedPokemons
  }

}
