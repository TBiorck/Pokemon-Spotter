import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpottedViewComponent } from './spotted-view.component';

describe('SpottedViewComponent', () => {
  let component: SpottedViewComponent;
  let fixture: ComponentFixture<SpottedViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpottedViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpottedViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
