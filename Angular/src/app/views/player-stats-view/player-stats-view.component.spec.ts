import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerStatsViewComponent } from './player-stats-view.component';

describe('PlayerStatsViewComponent', () => {
  let component: PlayerStatsViewComponent;
  let fixture: ComponentFixture<PlayerStatsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerStatsViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerStatsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
