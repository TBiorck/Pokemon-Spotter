import { Component, OnInit } from '@angular/core';
import { BackendService } from 'src/app/services/backend/backend.service';

@Component({
  selector: 'app-player-stats-view',
  templateUrl: './player-stats-view.component.html',
  styleUrls: ['./player-stats-view.component.scss']
})
export class PlayerStatsViewComponent implements OnInit {

  username: string = window.localStorage.getItem('user')
  score: string = ''
  rank: string = 'Unranked'

  scoreIsLoaded: boolean = false
  rankIsLoaded: boolean = false

  constructor(private backendService: BackendService) { }

  async ngOnInit(): Promise<any> {
    this.getPlayerScoreFromDatabase()
    this.getPlayerRankFromDatabase()
  }

  async getPlayerScoreFromDatabase() {
    try {
      const response: any  = await this.backendService.getUser(this.username)

      this.score = response.score

      this.scoreIsLoaded = true
      
    } catch (error) {
      console.error(error);
    }
  }

  async getPlayerRankFromDatabase() {
    try {
      const rank: any = await this.backendService.getPlayerRank(this.username)

      this.rank = rank
      this.rankIsLoaded = true
      
    } catch (error) {
      console.error(error);
    }
  }

}
