import { Component, OnInit } from '@angular/core';
import { BackendService } from 'src/app/services/backend/backend.service';
import { PokeapiService } from 'src/app/services/pokeapi/pokeapi.service';

@Component({
  selector: 'app-anonymous-view',
  templateUrl: './anonymous-view.component.html',
  styleUrls: ['./anonymous-view.component.scss']
})
export class AnonymousViewComponent implements OnInit {

  constructor(private pokeapi: PokeapiService,private backendService: BackendService) { }

  top10Players: Array<Object> = []
  rarestPokemons: Array<Object> = []

  playersAreLoaded: boolean = false
  pokemonsAreLoaded: boolean = false

  ngOnInit(): void {
    this.getTopPlayersFromDatabase()
    this.getRarestPokemonsFromDatabase()
  }

  private async getTopPlayersFromDatabase() {
    try {
      const response: any = await this.backendService.getTop10Players()      
      this.top10Players = response
      this.playersAreLoaded = true
    } catch (error) {
      console.error(error);
    }
  }

  private async getRarestPokemonsFromDatabase() {
    try {
      const fetchedPokemons: any = await this.backendService.getTop10RarestPokemon()

      // Get Pokemon data from PokeAPI then save to this.rarestPokemons
      for (let pokemon of fetchedPokemons) {
        const pokemonData = await this.pokeapi.getPokemonByNumber(pokemon.id)
        
        this.rarestPokemons.push({
          name: pokemonData.name,
          spottedCount: pokemon.seen
        })
      }
      this.pokemonsAreLoaded = true
    } catch (error) {
      console.error(error);
    }
  }
}
