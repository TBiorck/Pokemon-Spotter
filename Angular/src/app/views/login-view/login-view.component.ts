import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service'
import { BackendService } from '../../services/backend/backend.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-view',
  templateUrl: './login-view.component.html',
  styleUrls: ['./login-view.component.scss']
})
export class LoginViewComponent implements OnInit {

  constructor(private auth: AuthService, private backendService: BackendService, private router: Router) { }

  public showForm: boolean = false;
  public formText: string = "";
  public formSubmitText: string = "";
  public formPassword: string = "abcd";
  public formNewUser: boolean = false;

  async ngOnInit(): Promise<any> {
    if (window.location.hash && this.hasAccessToken(location.hash)){
      this.saveTokenToLocalStorage();

      await this.saveUserDataToLocalStorage(window.localStorage.getItem('access_token'));

    } else if(!window.localStorage.getItem('access_token')) {
      console.log("Needs to aquire access token form gitlab...");
      this.auth.login();
    }

    const username = window.localStorage.getItem('user');
    if(username) {
      try {
        await this.backendService.getUser(username);
        console.log("Account exists! Enter login details")
        this.showForm = true;
        this.formText = "Login";
        this.formSubmitText = "Submit";
        this.formNewUser = false;
      } catch(e) {
        console.error(e);
        console.log("A new user needs to be created!");
        this.showForm = true;
        this.formText = "Choose a password";
        this.formSubmitText = "Create User";
        this.formNewUser = true;
      }
    } else {
      console.error("Username don't exist in local storage?");
    }
  }

  login() {
    this.auth.login()
  }

  keyChange(event) {
    this.formPassword = event.target.value;
  }

  async submitPassword() {
    console.log(this.formNewUser)
    if(this.formNewUser) {
      console.log("Saving user to database");
      await this.saveUserToDatabase(window.localStorage.getItem('user'), this.formPassword);
      console.log("...done?")
    } else {
      // This is where the password should be verified against the backend
      // But we ran out of time
    }
    this.router.navigate(['/anonymous']);
  }
  
  private saveTokenToLocalStorage(): void {
    const accessToken = this.getAccessTokenFromHashString(location.hash);
    window.localStorage.setItem('access_token', accessToken)
  }

  private async saveUserDataToLocalStorage(accessToken: string): Promise<any> {
    const userData = await this.getUserDataByAccessToken(accessToken)
    window.localStorage.setItem('user', userData.email)
  }

  private getAccessTokenFromHashString(hashString: string) {
    let searchParams = new URLSearchParams(hashString.substring(1))
    return searchParams.get('access_token')
  }

  private async getUserDataByAccessToken(accessToken: string): Promise<any> {
    const response = await fetch('https://gitlab.com/api/v4/user', {
      headers: {
        Authorization: `Bearer ${accessToken}`
      }
    })
        
    return await response.json()
  }

  private async saveUserToDatabase(username: string, password: string) {
    console.log("Starting...");
    const user = {
      username: username,
      password: password
    }

    try {
      const response = await this.backendService.createUser(user)
      console.log(response); // TODO: remove/do anything with response message
      
    } catch (error) {
      console.error(error);
      
    }
    console.log("Finished!");
  }

  hasAccessToken(hashString: string): boolean {
    return new URLSearchParams(hashString.substring(1)).has('access_token')
  }
}
