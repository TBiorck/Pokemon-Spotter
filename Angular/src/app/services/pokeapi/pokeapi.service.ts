import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PokeapiService {

  constructor(private http: HttpClient) { }

  getPokemonByNumber(pokemonId): Promise<any> {
    return this.http.get(`${environment.pokeapiUrl}/pokemon/${pokemonId}`).toPromise();
  }

  getRandomPokemon() {
    return this.getPokemonByNumber(this.genererateRandomPokemonId())
  }

  private genererateRandomPokemonId () {
    const minPokemonId = 1
    const maxPokemonId = 151
    return Math.round(Math.random() * (maxPokemonId - minPokemonId) + minPokemonId);
  }
}
