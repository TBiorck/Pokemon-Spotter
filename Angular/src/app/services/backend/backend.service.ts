import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  constructor(private http: HttpClient) { }

  createUser(user) {
    return this.http.post(`${environment.backendApiUrl}/player/create`, user).toPromise()
  }


  getUser(username): Promise<any> {
    return this.http.get(`${environment.backendApiUrl}/player/get?username=${username}`).toPromise()
  }


  
  addSpottedPokemon(spottedPokemon: any): Promise<any> {
    return this.http.post(`${environment.backendApiUrl}/spotted`, spottedPokemon, { responseType: 'text'}).toPromise()
  }

  getPlayerRank(username) {
    return this.http.get(`${environment.backendApiUrl}/player/rank?username=${username}`).toPromise()
  }

  getTop10Players() {
    return this.http.get(`${environment.backendApiUrl}/player/top`).toPromise()
  }

  getTop10RarestPokemon() {
    return this.http.get(`${environment.backendApiUrl}/spotted/rarest`).toPromise()
  }
}
