import { Injectable } from '@angular/core';
import authConfig from '../../auth.config'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  login() {
    window.location.href = `https://gitlab.com/oauth/authorize?${new URLSearchParams(authConfig).toString()}`
  }
}
