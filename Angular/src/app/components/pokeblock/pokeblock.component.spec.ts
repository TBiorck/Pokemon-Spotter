import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PokeblockComponent } from './pokeblock.component';

describe('PokeblockComponent', () => {
  let component: PokeblockComponent;
  let fixture: ComponentFixture<PokeblockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PokeblockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PokeblockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
