package experis.se.PokemonSpotter.repositories;

import experis.se.PokemonSpotter.models.GlobalSeenPokemon;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface GlobalSeenPokemonRepository extends JpaRepository<GlobalSeenPokemon, Integer> {
    Optional<GlobalSeenPokemon> findByPokemonId(Integer id);
    GlobalSeenPokemon getByPokemonId(Integer id);
}
