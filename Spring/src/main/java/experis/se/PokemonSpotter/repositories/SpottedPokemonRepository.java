package experis.se.PokemonSpotter.repositories;

import experis.se.PokemonSpotter.models.Player;
import experis.se.PokemonSpotter.models.SpottedPokemon;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpottedPokemonRepository  extends JpaRepository<SpottedPokemon, Integer> {
   SpottedPokemon getById(Integer id);
}
