package experis.se.PokemonSpotter.models;

/*
* Pojo model for the data that the frontend provides
* */

public class ReceivedSpottedPokemon {
   public String caughtBy;
   public String id;
   public String latitude;
   public String longitude;
   public String gender;
   public boolean shiny;
}
