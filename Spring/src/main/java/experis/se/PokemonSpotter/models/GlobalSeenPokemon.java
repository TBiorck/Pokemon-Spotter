package experis.se.PokemonSpotter.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

/**
 * Model for the "rarity" of a pokemon as decided by the amount it has been
 * reported "seen". It is mainly used as a tool to check rarity.
 * */

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class GlobalSeenPokemon {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    @Column(nullable = false)
    public Integer pokemonId;
    @Column(nullable = false)
    public int seen;
}
