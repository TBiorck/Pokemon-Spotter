package experis.se.PokemonSpotter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PokemonSpotterApplication {

	public static void main(String[] args) {
		SpringApplication.run(PokemonSpotterApplication.class, args);
	}

}
